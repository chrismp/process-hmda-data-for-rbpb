inputDir <- "../county-tract/"
outputDir <- "output/"
year <- "2017"

loanTypes <- c(
  "improvement",
  "purchase",
  "refinance"
)

for(loanType in loanTypes){
  inputFiles <- list.files(
    path = inputDir,
    pattern = paste0(year,"_home-",loanType,"-loans_")
  )
  for(inputFile in inputFiles){
    inputFilepath <- paste0(inputDir,inputFile)
    rawDfName <- gsub(
      x = inputFile,
      pattern = ".csv",
      replacement = ''
    )
    assign(
      x = rawDfName,
      value = read.csv(
        file = inputFilepath,
        colClasses = "character"
      )
    )
    df <- get(rawDfName)
    df$loans_per_10K <- (as.numeric(df$count_loan_purpose_name) / as.numeric(df$sum_population)) * 10000
    df$loans_per_10K_round <- round(x = df$loans_per_10K, digits = 1)
    write.csv(
      x = df,
      file = paste0(outputDir,rawDfName,".csv"),
      row.names = F
    )
  }
}